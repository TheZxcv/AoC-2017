#include <algorithm>
#include <cassert>
#include <iostream>
#include <vector>

struct Point3 {
	int x, y, z;

	Point3() : x(0), y(0), z(0) {}
	Point3(int _x, int _y, int _z) : x(_x), y(_y), z(_z) {}
	~Point3() {}

	int distance(const Point3& p) const {
		return abs(this->x - p.x) + abs(this->y - p.y) + abs(this->z - p.z);
	}

	int length() const {
		return abs(this->x) + abs(this->y) + abs(this->z);
	}

	friend Point3& operator+=(Point3& a, const Point3& b) {
		a.x += b.x;
		a.y += b.y;
		a.z += b.z;
		return a;
	}

	friend bool operator==(const Point3& a, const Point3& b) {
		return a.x == b.x && a.y == b.y && a.z == b.z;
	}

	friend std::istream& operator>>(std::istream& is, Point3& p) {
		char ch;
		is >> ch;
		assert(ch == '<');
		is >> p.x;
		is >> ch;
		assert(ch == ',');
		is >> p.y;
		is >> ch;
		assert(ch == ',');
		is >> p.z;
		is >> ch;
		assert(ch == '>');
		return is;
	}

	friend std::ostream& operator<<(std::ostream& os, const Point3& p) {
		os << "<" << p.x << "," << p.y << "," << p.z << ">";
		return os;
	}
};

struct Particle {
	int id = 0;
	Point3 p;
	Point3 v;
	Point3 a;

	~Particle() {}

	void update() {
		v += a;
		p += v;
	}

	Point3 next() {
		Point3 copy_v = v;
		copy_v += a;
		Point3 copy_p = p;
		copy_p += copy_v;
		return copy_p;
	}

	friend std::istream& operator>>(std::istream& is, Particle& p) {
		char ch;
		is >> ch;
		if (ch != 'p') goto fail;
		is >> ch;
		if (ch != '=') goto fail;
		is >> p.p;

		is >> ch;
		if (ch != ',') goto fail;

		do is >> ch; while (ch == ' ');
		if (ch != 'v') goto fail;
		is >> ch;
		if (ch != '=') goto fail;
		is >> p.v;

		is >> ch;
		if (ch != ',') goto fail;

		do is >> ch; while (ch == ' ');
		if (ch != 'a') goto fail;
		is >> ch;
		if (ch != '=') goto fail;
		is >> p.a;

		return is;
fail:
		is.setstate(std::ios_base::failbit);
		return is;
	}

	friend std::ostream& operator<<(std::ostream& os, const Particle& p) {
		os << "p=" << p.p << ", v=" << p.v << ", a=" << p.a;	
		return os;
	}
};

void first(std::vector<Particle> particles) {
	Point3 center{0,0,0};
	bool all_moving_away;
	do {
		for (Particle& p : particles) {
			p.update();
		}
	
		all_moving_away = true;
		for (Particle& p : particles) {
			if (center.distance(p.p) > center.distance(p.next())) {
				all_moving_away = false;
				break;
			}
		}
	} while (!all_moving_away);


	const Particle *min = &particles[0];
	for (Particle& p : particles) {
		if (min->a.length() > p.a.length()) {
			min = &p;
		}
	}
	std::cout << "[1] Forever closest: " << min->id << std::endl;
}

void second(std::vector<Particle> particles) {
	int cnt = 0;
	while (cnt < 10000) {
		std::for_each(begin(particles), end(particles), [] (Particle& p) { if (p.id != -1) p.update(); });

		cnt++;
		for (auto it = begin(particles); it != end(particles); ++it) {
			if (it->id != -1) {
				for (auto it2 = std::next(it); it2 != end(particles); ++it2) {
					if (it2->id != -1 && it->p == it2->p) {
						it->id = -1;
						it2->id = -1;
						cnt = 0;
					}
				}
			}
		}
	}

	int nleft = 0;
	for (auto it : particles) {
		if (it.id != -1) {
			nleft++;
		}
	}
	std::cout << "[2] Particles left: " << nleft << std::endl;
}

int main() {
	std::vector<Particle> particles{};

	Particle part;
	int id = 0;
	while (std::cin >> part) {
		part.id = id++;
		particles.push_back(part);
	}

	first(particles);
	second(particles);

	return 0;
}
