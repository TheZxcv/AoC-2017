#include "../10/knots.h"

#include <algorithm>
#include <bitset>
#include <iostream>

bool is_valid(int i, int j) {
	return i >= 0 && i < 128 && j >= 0 && j < 128;
}

void zero_all_neighbours(int i, int j, std::array<std::array<bool, 128>, 128>& mat) {
	mat[i][j] = false;
	
	if (is_valid(i+1, j) && mat[i+1][j])
		zero_all_neighbours(i+1, j, mat);
	if (is_valid(i-1, j) && mat[i-1][j])
		zero_all_neighbours(i-1, j, mat);
	if (is_valid(i, j+1) && mat[i][j+1])
		zero_all_neighbours(i, j+1, mat);
	if (is_valid(i, j-1) && mat[i][j-1])
		zero_all_neighbours(i, j-1, mat);
}

int main() {
	int used = 0;
	std::array<std::array<bool, 128>, 128> mat{};
	std::string key;
	if (getline(std::cin, key)) {
		std::cout << "Key: '" << key << "'" << std::endl;
		std::vector<int> data{};

		for (int i = 0; i < 128; i++) {
			data.clear();
			for (char c : key) {
				data.push_back(c);
			}
			data.push_back('-');
			for (char c : std::to_string(i)) {
				data.push_back(c);
			}

			int j = 0;
			auto hash = knot_hash(data);
			for (uint8_t e : hash) {
				used += std::bitset<8>{e}.count();
				
				uint8_t mask = 0x80;
				while (mask) {
					mat[i][j++] = ((e & mask) != 0);
					mask >>= 1;
				}
			}
		}
	}
	std::cout << "Used blocks: " << used << std::endl;

	int nregions = 0;
	for (int i = 0; i < 128; i++) {
		for (int j = 0; j < 128; j++) {
			if (mat[i][j]) {
				zero_all_neighbours(i, j, mat);
				nregions++;
			}
		}
	}
	std::cout << "Regions: " << nregions << std::endl;
	
	return 0;
}
