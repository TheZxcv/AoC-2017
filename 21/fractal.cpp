#include <cassert>
#include <cstdint>
#include <iostream>
#include <map>
#include <sstream>
#include <vector>

using std::begin;
using std::end;

uint16_t flipV_2x2(const uint16_t grid) {
	return ((grid&0x000c) >> 2) | ((grid&0x0003) << 2);
}

uint16_t flipV_3x3(const uint16_t grid) {
	return ((grid&0x01c0) >> 6) | (grid&0x0038) | ((grid&0x0007) << 6);
}

uint16_t rotate90_2x2(const uint16_t grid) {
	return ((grid&0x0008) >> 1)
		| ((grid&0x0004) >> 2)
		| ((grid&0x0002) << 2)
		| ((grid&0x0001) << 1);
}

uint16_t rotate90_3x3(const uint16_t grid) {
	return ((grid&0x0100) >> 2)
		| ((grid&0x0080) >> 4)
		| ((grid&0x0040) >> 6)
		| ((grid&0x0020) << 2)
		| ((grid&0x0010))
		| ((grid&0x0008) >> 2)
		| ((grid&0x0004) << 6)
		| ((grid&0x0002) << 4)
		| ((grid&0x0001) << 2);
}

void print2x2(const uint16_t grid) {
	uint16_t mask = 0x0008;
	while (mask != 0) {
		for (int i = 0; i < 2; i++) {
			char ch = ((grid&mask) != 0) ? '#' : '.';
			std::cout << ch;
			mask >>= 1;
		}
		std::cout << std::endl;
	}
}

void print3x3(const uint16_t grid) {
	uint16_t mask = 0x0100;
	while (mask != 0) {
		for (int i = 0; i < 3; i++) {
			char ch = ((grid&mask) != 0) ? '#' : '.';
			std::cout << ch;
			mask >>= 1;
		}
		std::cout << std::endl;
	}
}

uint16_t convert2x2(const std::vector<std::string>& mat, int i, int j) {
	uint16_t res = 0;
	if (mat[ i ].at( j ) == '#') res |= 0x0008;
	if (mat[ i ].at(j+1) == '#') res |= 0x0004;
	if (mat[i+1].at( j ) == '#') res |= 0x0002;
	if (mat[i+1].at(j+1) == '#') res |= 0x0001;
	return res;
}

uint16_t convert3x3(const std::vector<std::string>& mat, int i, int j) {
	uint16_t res = 0;
	if (mat[ i ].at( j ) == '#') res |= 0x0100;
	if (mat[ i ].at(j+1) == '#') res |= 0x0080;
	if (mat[ i ].at(j+2) == '#') res |= 0x0040;
	if (mat[i+1].at( j ) == '#') res |= 0x0020;
	if (mat[i+1].at(j+1) == '#') res |= 0x0010;
	if (mat[i+1].at(j+2) == '#') res |= 0x0008;
	if (mat[i+2].at( j ) == '#') res |= 0x0004;
	if (mat[i+2].at(j+1) == '#') res |= 0x0002;
	if (mat[i+2].at(j+2) == '#') res |= 0x0001;
	return res;
}

uint16_t toBitmat_2x2(const std::string& pattern) {
	uint16_t res = 0;
	uint16_t mask = 0x0008;
	for (char ch : pattern) {
		if (ch == '#' || ch == '.') {
			if (ch == '#')
				res |= mask;
			mask >>= 1;
		}
	}
	return res;
}

uint16_t toBitmat_3x3(const std::string& pattern) {
	uint16_t res = 0;
	uint16_t mask = 0x0100;
	for (char ch : pattern) {
		if (ch == '#' || ch == '.') {
			if (ch == '#')
				res |= mask;
			mask >>= 1;
		}
	}
	return res;
}

std::string search2(const std::map<uint16_t, std::string>& mappings, uint16_t key) {
	uint16_t flipped = flipV_2x2(key);
	uint16_t keys[8] = {
		key,
		rotate90_2x2(key),
		rotate90_2x2(rotate90_2x2(key)),
		rotate90_2x2(rotate90_2x2(rotate90_2x2(key))),
		flipped,
		rotate90_2x2(flipped),
		rotate90_2x2(rotate90_2x2(flipped)),
		rotate90_2x2(rotate90_2x2(rotate90_2x2(flipped))),
	};

	for (uint16_t el : keys) {
		auto it = mappings.find(el);
		if (it != end(mappings)) {
			return it->second;
		}
	}
	abort();
}

std::string search3(const std::map<uint16_t, std::string>& mappings, uint16_t key) {
	uint16_t flipped = flipV_3x3(key);
	uint16_t keys[8] = {
		key,
		rotate90_3x3(key),
		rotate90_3x3(rotate90_3x3(key)),
		rotate90_3x3(rotate90_3x3(rotate90_3x3(key))),
		flipped,
		rotate90_3x3(flipped),
		rotate90_3x3(rotate90_3x3(flipped)),
		rotate90_3x3(rotate90_3x3(rotate90_3x3(flipped))),
	};

	for (uint16_t el : keys) {
		auto it = mappings.find(el);
		if (it != end(mappings)) {
			return it->second;
		}
	}
	abort();
}

void expand_2x2(std::map<uint16_t, std::string> mappings, std::vector<std::string>& mat) {
	std::vector<std::string> blocks{};
	for (size_t i = 0; i < mat.size(); i += 2) {
		for (size_t j = 0; j < mat.size(); j += 2) {
			blocks.push_back(search2(mappings, convert2x2(mat, i, j)));
		}
	}

	size_t new_size = (mat.size() / 2) * 3;
	for (size_t i = 0; i < mat.size(); i++)
		mat[i].resize(new_size);
	for (size_t i = mat.size(); i < new_size; i++)
		mat.emplace_back(new_size, 'x');

	assert(mat.size() == mat[0].size());
	int blkid = 0;
	for (size_t i = 0; i < mat.size(); i += 3) {
		for (size_t j = 0; j < mat[i].size(); j += 3) {
			std::string block = blocks[blkid++];
			assert(block.size() == 11);
			mat[ i ][ j ] = block[0];
			mat[ i ][j+1] = block[1];
			mat[ i ][j+2] = block[2];

			mat[i+1][ j ] = block[4];
			mat[i+1][j+1] = block[5];
			mat[i+1][j+2] = block[6];

			mat[i+2][ j ] = block[8];
			mat[i+2][j+1] = block[9];
			mat[i+2][j+2] = block[10];
		}
	}
}

void expand_3x3(std::map<uint16_t, std::string> mappings, std::vector<std::string>& mat) {
	std::vector<std::string> blocks{};
	for (size_t i = 0; i < mat.size(); i += 3) {
		for (size_t j = 0; j < mat.size(); j += 3) {
			blocks.push_back(search3(mappings, convert3x3(mat, i, j)));
		}
	}

	size_t new_size = (mat[0].size() / 3) * 4;
	for (size_t i = 0; i < mat.size(); i++)
		mat[i].resize(new_size, 'x');
	for (size_t i = mat.size(); i < new_size; i++)
		mat.emplace_back(new_size, 'x');

	assert(mat.size() == mat[0].size());
	int blkid = 0;
	for (size_t i = 0; i < mat.size(); i += 4) {
		for (size_t j = 0; j < mat[i].size(); j += 4) {
			std::string block = blocks[blkid++];
			assert(block.size() == 19);
			mat[ i ][ j ] = block[0];
			mat[ i ][j+1] = block[1];
			mat[ i ][j+2] = block[2];
			mat[ i ][j+3] = block[3];

			mat[i+1][ j ] = block[5];
			mat[i+1][j+1] = block[6];
			mat[i+1][j+2] = block[7];
			mat[i+1][j+3] = block[8];

			mat[i+2][ j ] = block[10];
			mat[i+2][j+1] = block[11];
			mat[i+2][j+2] = block[12];
			mat[i+2][j+3] = block[13];

			mat[i+3][ j ] = block[15];
			mat[i+3][j+1] = block[16];
			mat[i+3][j+2] = block[17];
			mat[i+3][j+3] = block[18];
		}
	}
}

int main() {
	std::vector<std::string> mat{{
		".#.",
		"..#",
		"###"
	}};
	std::map<uint16_t, std::string> mappings2{};
	std::map<uint16_t, std::string> mappings3{};
	
	std::string line;
	while (std::getline(std::cin, line)) {
		std::istringstream iss{line};

		std::string pattern;
		iss >> pattern;
		uint16_t key;
		if (pattern.size() == 5) {
			key = toBitmat_2x2(pattern);
			iss >> pattern;
			iss >> pattern;
			mappings2[key] = pattern;
		} else if (pattern.size() == 11) {
			key = toBitmat_3x3(pattern);
			iss >> pattern;
			iss >> pattern;
			mappings3[key] = pattern;
		} else
			abort();
	}

	int onesAt5 = 0, onesAt18 = 0;
	for (int i = 0; i < 18; i++) {
		if (mat.size()%2 == 0)
			expand_2x2(mappings2, mat);
		else if (mat.size()%3 == 0)
			expand_3x3(mappings3, mat);
		else
			abort();

		if (i == 4) {
			for (auto row : mat) {
				for (char ch : row) {
					if (ch == '#') onesAt5++;
				}
			}
		}
	}

	for (auto row : mat) {
		for (char ch : row) {
			if (ch == '#') onesAt18++;
		}
	}
	std::cout << "[1] After 5 iterations there are " << onesAt5 << " pixels on." << std::endl;
	std::cout << "[2] After 18 iterations there are " << onesAt18 << " pixels on." << std::endl;

	return 0;
}
