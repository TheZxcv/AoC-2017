#include "knots.h"

#include <algorithm>

using std::begin;
using std::end;

bool fixed_round = true;

void enable_fixed_round() {
	fixed_round = true;
}

void disable_fixed_round() {
	fixed_round = false;
}

const std::array<uint8_t, 16>& knot_hash(const std::vector<int>& data) {
	int i = 0;
	const int rounds = 64;
	std::array<int, 256> init{};
	std::generate(begin(init), end(init), [&i] () { return i++; });
	return knot_hash(init, data, rounds);
}

