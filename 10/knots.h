#ifndef KNOTS_H
#define KNOTS_H


#include <algorithm>
#include <array>
#include <cassert>
#include <cstdint>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>

extern bool fixed_round;

template <class Iterator>
void reverse(Iterator begin, Iterator end, Iterator starting_point, Iterator ending_point, size_t length) {
	for (size_t i = 0; i < length; i+=2) {
		std::iter_swap(starting_point, ending_point);

		starting_point++;
		if (starting_point == end) {
			starting_point = begin;
		}

		if (ending_point == begin) {
			ending_point = end;
		}
		ending_point--;
	}
}

template<size_t N>
void knot_hash_step(std::array<int, N>& elements, size_t current_pos, int length) {
	size_t ending_pos = (current_pos + length - 1) % elements.size(); 
	reverse(
		begin(elements), end(elements),
		std::next(begin(elements), current_pos), std::next(begin(elements), ending_pos),
		length
	);
}

const std::array<uint8_t, 16>& knot_hash(const std::vector<int>& data);

void enable_fixed_round();
void disable_fixed_round();

template<size_t N>
int knot_hash_rounds(std::array<int, N>& elements, const std::vector<int>& lengths, int rounds) {
	static std::array<int, 5> fixed_lengths{17, 31, 73, 47, 23};

	size_t current_pos = 0;
	size_t skip_size = 0;
	for (int i = 0; i < rounds; i++) {
		for (auto length : lengths) {
			knot_hash_step(elements, current_pos, length);
			current_pos = (current_pos + length + skip_size) % elements.size();
			skip_size = (skip_size + 1) % elements.size();
		}
		if (fixed_round) {
			for (auto length : fixed_lengths) {
				knot_hash_step(elements, current_pos, length);
				current_pos = (current_pos + length + skip_size) % elements.size();
				skip_size = (skip_size + 1) % elements.size();
			}
		}
	}

	return elements[0] * elements[1];
}

template<size_t N>
const std::array<uint8_t, 16>& knot_hash(std::array<int, N>& elements, const std::vector<int>& lengths, int rounds) {
	static std::array<uint8_t, 16> res{};
	knot_hash_rounds(elements, lengths, rounds);

	for (size_t i = 0; i < elements.size(); i += 16) {
		int digit = 0;
		for (size_t j = i; j < i + 16; j++) {
			digit ^= elements[j];
		}
		res[i / 16] = digit;
	}
	
	return res;
}

template<size_t N>
std::string knot_hash(std::array<int, N>& elements, const std::vector<int>& lengths, int rounds, std::string& output) {
	auto res = knot_hash(elements, lengths, rounds);

	std::stringstream ss;
	for (uint8_t digit : res) {
		ss << std::setfill('0') << std::setw(2) << std::hex << (int) digit;
	}
	
	output = ss.str();
	return output;
}

#endif
