#include "knots.h"

void first(std::istream& in) {
	disable_fixed_round();
	std::array<int, 256> list{};
	int i = 0;
	std::generate(begin(list), end(list), [&i] () { return i++; });

	std::vector<int> lens;
	std::string input;
	while (getline(in, input, ',')) {
		lens.push_back(std::stoi(input));
	}

	std::cout << "Product of the first two elements: " << knot_hash_rounds(list, lens, 1) << std::endl;
}

void second(std::istream& in) {
	// Uncomment to hash "AoC 2017"
	// in.unsetf(std::ios_base::skipws);
	enable_fixed_round();
	const int rounds = 64;
	std::array<int, 256> list{};
	int i = 0;
	std::generate(begin(list), end(list), [&i] () { return i++; });

	std::vector<int> lens;
	char ch;
	while (in >> ch) {
		lens.push_back(ch);
	}

	std::string hash;
	std::cout << "Product of the first two elements: " << knot_hash(list, lens, rounds, hash) << std::endl;
}


int main() {
	//first(std::cin);
	second(std::cin);
	return 0;
}
