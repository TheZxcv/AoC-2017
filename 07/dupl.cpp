#include <cassert>
#include <iostream>
#include <regex>
#include <set>
#include <sstream>
#include <map>

// txszqu (687) -> mvjqmad, lwqlyjq, jlgnsu

struct Node {
    std::string id;
    int weight;
    std::vector<std::string> children;
};

const Node parse_node(const std::string& line) {
    static std::regex word_regex{"([a-z]+)"};
    static std::regex node_regex{"([a-z]+) \\(([0-9]+)\\)(?: -> ([a-z ,]+))?"};
    auto i = std::sregex_iterator(line.begin(), line.end(), node_regex);
    std::smatch match = *i;
    const std::string& name = match.str(1);
    int weight = std::stoi(match.str(2));
    const std::string& rest = match.str(3);

    std::vector<std::string> children{};
    auto iter_begin = std::sregex_iterator(rest.begin(), rest.end(), word_regex);
    auto iter_end   = std::sregex_iterator();

    for (auto i = iter_begin; i != iter_end; ++i) {
        children.push_back(i->str(1));
    }

    return Node{name, weight, children};
}

int calc_weight(const std::map<std::string, Node>& nodes, const Node& root) {
    int weight = root.weight;

    for (auto id : root.children) {
        const Node& node = nodes.at(id);
        weight += calc_weight(nodes, node);
    }
    return weight;
}

int find_different(int *vals, size_t len) {
    if (len == 0) {
        return -1;
    }
    int a = 0;
    int adupls = 0;
    int b = -1;
    for (size_t i = 1; i < len; i++) {
        if (vals[a] != vals[i]) {
            b = i;
            break;
        } else {
            adupls++;
        }
    }

    if (adupls > 0 || b == -1) {
        return b;
    }
    for (size_t i = 1; i < len; i++) {
        if (vals[a] == vals[i]) {
            return b;
        }
    }
    return a;
}

void search_wrong_node(const std::map<std::string, Node>& nodes, const Node& root) {
    static int values[1024];

    int i = 0;
    int difference = 0;
    const Node *curr = &root;
    do {
        int cnt = 0;
        for (auto id : curr->children) {
            const Node& node = nodes.at(id);
            int weight = calc_weight(nodes, node);
            values[cnt++] = weight;
        }
        i = find_different(values, curr->children.size());
        if (i != -1) {
            int other = (i + 1)%curr->children.size();
            curr = &nodes.at(curr->children[i]);
            difference = values[other] - values[i];
        }
    } while (i != -1);
    std::cout << curr->weight + difference << std::endl;
}

int main() {
    std::string line;
    std::set<std::string> words{};
    std::map<std::string, Node> nodes{};
    while (std::getline(std::cin, line)) {
        const Node& node = parse_node(line);
        nodes[node.id] = node;

        if (words.find(node.id) != words.end()) {
            words.erase(node.id);
        } else {
            words.insert(node.id);
        }
        for (auto word : node.children) {
            if (words.find(word) != words.end()) {
                words.erase(word);
            } else {
                words.insert(word);
            }
        }
    }

    assert(words.size() == 1);
    std::string root_id = *words.begin();
    std::cout << "The root is " << root_id << std::endl;

    const Node& root = nodes[root_id];
    search_wrong_node(nodes, root);
    return 0;
}