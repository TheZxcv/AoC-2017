#include <algorithm>
#include <iostream>
#include <set>
#include <string>
#include <sstream>

bool is_valid(const std::string& line) {
	std::set<std::string> words{};
	std::istringstream sline{line};
	std::string curr;
	size_t nwords = 0;
	while (sline >> curr) {
		std::sort(curr.begin(), curr.end());
		words.insert(curr);
		nwords++;
	}
	return nwords == words.size();
}

bool is_valid2(const std::string& line) {
	std::set<std::string> words{};
	std::istringstream sline{line};
	std::string curr;
	size_t nwords = 0;
	while (sline >> curr) {
		words.insert(curr);
		nwords++;
	}
	return nwords == words.size();
}

int count_valid_passwords(std::istream &input, bool (*is_valid)(const std::string&)) {
	int cnt = 0;
	std::string line;
	while (std::getline(input, line)) {
		if (is_valid(line)) {
			cnt++;
		}
	}
	return cnt;
}

int main() {
	int count = count_valid_passwords(std::cin, is_valid2);
	std::cout << "Valid passwords: " << count << std::endl;
	return 0;
}