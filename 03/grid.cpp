#include <iostream>
#include <cmath>
#include <vector>

int len_shortest_path(int position) {
  int num = position - 1;
  int layer = 0;
  int bottom = 0;
  int top = 0;

  while (top < num) {
    bottom = top;
    layer++;
    top += 8*layer;
  }

  int inner_len = (8*layer - 4) / 4;

  int dist = 8*layer;
  for (int cen = (bottom + 1) + (inner_len - 1) / 2;
        cen < top;
        cen += (8*layer) / 4) {
        if (dist > abs(num - cen)) {
          dist = abs(num - cen);
        }
    }

  if (dist == 8*layer) {
    dist = 0;
  }
  return layer + dist;
}

int main() {
  std::vector<int> values{1, 12, 23, 1024, 361527};
  for (auto i : values) {
    std::cout << i << ": " <<  len_shortest_path(i) << std::endl;
  }
  // part 2??
  return 0;
}
