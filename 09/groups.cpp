#include <cassert>
#include <iostream>
#include <sstream>

class ScoreEvaluator {
public:
  int garbage_count = 0;

  ScoreEvaluator(std::istream& input) : input(input) {}
  ~ScoreEvaluator() {}

  int eval_score() {
    return this->read_groups();
  }

private:
  int currentDepth = 0;
  std::istream& input;

  char getc() {
    return input.get();
  }

  void begin_garbage() {
    char ch;
    do {
      ch = getc();
      if (ch == '!') {
        (void) getc();
        continue;
      } else if (ch != '>') {
        garbage_count++;
      }
    } while(ch != '>');
  }

  int begin_group() {
    currentDepth++;
    int score = currentDepth;

    char ch;
    do {
      ch = getc();
      switch(ch) {
        case '{':
          score += begin_group();
          ch = getc();
          break;
        case '<':
          begin_garbage();
          ch = getc();
          break;
        case '}': /* do nothing */
          break;
        default:
          printf("Got unexpected character: `%c`.", ch);
          abort();
      }
    } while(ch == ',');
    assert(ch == '}');
    currentDepth--;
    return score;
  }

  int read_groups() {
    currentDepth = 0;
    int totalScore = 0;
    char ch = getc();

    switch(ch) {
      case '{':
        totalScore = begin_group();
        break;
      case '<':
        begin_garbage();
        break;
      default:
        printf("Got unexpected character: `%c`.", ch);
        abort();
    }
    return totalScore;
  }
};

void test_score(const std::string& input, int expected) {
  std::istringstream iss{input};
  ScoreEvaluator se{iss};
  assert(se.eval_score() == expected);
}

void test_garbage_count(const std::string& input, int expected) {
  std::istringstream iss{input};
  ScoreEvaluator se{iss};
  (void) se.eval_score();
  assert(se.garbage_count == expected);
}

void test_all() {
  test_score("{}", 1);
  test_score("{{{}}}", 6);
  test_score("{{},{}}", 5);
  test_score("{{{},{},{{}}}}", 16);
  test_score("{<a>,<a>,<a>,<a>}", 1);
  test_score("{{<ab>},{<ab>},{<ab>},{<ab>}}", 9);
  test_score("{{<!!>},{<!!>},{<!!>},{<!!>}}", 9);
  test_score("{{<a!>},{<a!>},{<a!>},{<ab>}}", 3);

  test_garbage_count("<>", 0);
  test_garbage_count("<random characters>", 17);
  test_garbage_count("<<<<>", 3);
  test_garbage_count("<{!>}>", 2);
  test_garbage_count("<!!>", 0);
  test_garbage_count("<!!!>>", 0);
  test_garbage_count("<{o\"i!a,<{i<a>", 10);

  printf("\x1b[1;32mAll tests passed\x1b[0m\n");
}

int main() {
  test_all();
  ScoreEvaluator se{std::cin};
  std::cout << "Score: " << se.eval_score() << std::endl;
  std::cout << "Garbage count: " << se.garbage_count << std::endl;
  return 0;
}
