#include <algorithm>
#include <cassert>
#include <iostream>
#include <sstream>
#include <vector>

using std::begin;
using std::end;

struct layer {
	int depth;
	int range;
	
	layer(int d, int r) : depth(d), range(r) {}
};

int calc_pos(int range, int psec) {
	int pos = psec % (2*range - 2);
	return (pos < range) ? pos : (range - (pos - range) - 2);
}

int main() {
	std::vector<layer> layers{};
	int severity = 0;
	std::string line;
	while (std::getline(std::cin, line)) {
		std::istringstream iss{line};

		int depth;
		int range;
		char colon;
	
		iss >> depth;
		iss >> colon;
		assert(colon == ':');
		iss >> range;

		if (calc_pos(range, depth) == 0) {
			severity += depth * range;
		}
		layers.emplace_back(depth, range);
	}
	std::cout << "Severity: " << severity << std::endl;

	int delay = 0;
	bool caught;
	do {
		delay++;
		caught = std::any_of(begin(layers), end(layers),
			[delay] (const layer l) {
				return calc_pos(l.range, l.depth + delay) == 0;
		});
	} while(caught);
	std::cout << "Delay: " << delay << std::endl;

	return 0;
}
