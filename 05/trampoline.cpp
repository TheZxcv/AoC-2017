#include <iostream>
#include <vector>

int update_jmp_add_one(int jmp) {
    return jmp + 1;
}

int update_jmp_weird(int jmp) {
	return (jmp >= 3) ? jmp - 1 : jmp + 1;
}

size_t read_instrs(std::istream &is, std::vector<int>& instrs) {
	int curr;
	size_t cnt = 0;
	while (is >> curr) {
		instrs.push_back(curr);
		cnt++;
	}
	return cnt;
}

int computation(std::vector<int> instrs, int (*update_jmp)(int)) {
	int PC = 0;
	int nsteps = 0;
	int len = instrs.size();
	while (0 <= PC && PC < len) {
		int jmp = instrs[PC];
		instrs[PC] = update_jmp(jmp);
		PC += jmp;
		nsteps++;
	}
    return nsteps;
}

int main() {
    int nsteps;
	std::vector<int> instrs{};
	read_instrs(std::cin, instrs);

    nsteps = computation(instrs, update_jmp_add_one);
    std::cout << "1. it took " << nsteps << " steps." << std::endl;
    nsteps = computation(instrs, update_jmp_weird);
    std::cout << "2. it took " << nsteps << " steps." << std::endl;
	return 0;
}
