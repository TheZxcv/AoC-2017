#include <algorithm>
#include <cassert>
#include <iostream>
#include <list>

using std::begin;
using std::end;

int spinlock(int steps, int last_to_insert, int target) {
	std::list<int> ring{0};

	size_t current_pos = 0;
	int last;
	for (int i = 1; i <= last_to_insert; i++) {
		current_pos = (current_pos + steps) % i /* ring.size() */;
		ring.insert(std::next(begin(ring), current_pos + 1), i);
		current_pos++;
		if (current_pos == 1)
			last = i;
	}

	int value_after;
	auto ret = std::find(begin(ring), end(ring), target);
	assert(ret != end(ring));

	++ret;
	if (ret != end(ring)) {
		value_after = *ret;
	} else {
		value_after = *begin(ring);
	}
	return value_after;
	return last;
}

void first(int steps) {
	std::cout << "[1] Value after 2017: " << spinlock(steps, 2017, 2017) << std::endl;
}

void second(int steps) {
	size_t current_pos = 0;
	int after_zero = -1;
	for (int i = 1; i <= 5000000; i++) {
		current_pos = (current_pos + steps) % i + 1;
		if (current_pos == 1)
			after_zero = i;
	}
	std::cout << "[2] Value after 0: " << after_zero << std::endl;
}

int main() {
	int steps;
	std::cin >> steps;
	std::cout << "Steps: " << steps << std::endl;
	first(steps);
	second(steps);

	return 0;
}