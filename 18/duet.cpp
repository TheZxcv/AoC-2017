#include <algorithm>
#include <cctype>
#include <cstdint>
#include <iostream>
#include <map>
#include <memory>
#include <queue>
#include <sstream>
#include <vector>

using std::begin;
using int_reg = int64_t;
using Regs = std::map<char, int_reg>;

int_reg get_value(const Regs& regs, const std::string& tok) {
	if (std::isalpha(tok[0])) {
		auto val = regs.find(tok[0]);
		return (val != end(regs)) ? val->second : 0;
	} else {
		return std::stoi(tok);
	}
}

void set_value(Regs& regs, char reg, int_reg val) {
	regs[reg] = val;
}

void first(const std::vector<std::string>& prog) {
	Regs regs{};
	bool running = true;
	size_t PC = 0;
	int freq = 0;
	while (running && PC < prog.size()) {
		int pc_incr = 1;
		const std::string& curr = prog[PC];

		std::istringstream iss{curr};

		char reg;
		std::string op, tok, extra;
		iss >> op;
		if ("snd" == op) {
			iss >> tok;
			freq = get_value(regs, tok);
		} else if ("set" == op) {
			iss >> reg;
			iss >> tok;
			regs[reg] = get_value(regs, tok);
		} else if ("add" == op) {
			iss >> tok;
			iss >> extra;
			set_value(regs, tok[0], get_value(regs, tok) + get_value(regs, extra));
		} else if ("mul" == op) {
			iss >> tok;
			iss >> extra;
			set_value(regs, tok[0], get_value(regs, tok) * get_value(regs, extra));
		} else if ("mod" == op) {
			iss >> tok;
			iss >> extra;
			set_value(regs, tok[0], get_value(regs, tok) % get_value(regs, extra));
		} else if ("rcv" == op) {
			iss >> tok;
			if (get_value(regs, tok) != 0) {
				std::cout << "[1] Recovered frequency: " << freq << std::endl;
				return;
			}
		} else if ("jgz" == op) {
			iss >> tok;
			iss >> extra;
			if (get_value(regs, tok) > 0) {
				pc_incr = get_value(regs, extra);
			}
		} else {
			std::cout << "Invalid instruction: " << curr << std::endl;
		}

		PC += pc_incr;
	}
}

using queue = std::queue<int_reg>;
class DuetMachine {
public:
	int send_calls = 0;
	bool running = true;

	DuetMachine(int id, const std::vector<std::string>& program,
			std::shared_ptr<queue> in_queue, std::shared_ptr<queue> out_queue)
		: PC(0), prog(program), in(in_queue), out(out_queue) {
		regs['p'] = id;
	}

	void execute() {
		while (running && PC < prog.size()) {
			int pc_incr = 1;
			const std::string& curr = prog[PC];

			std::istringstream iss{curr};

			char reg;
			std::string op, tok, extra;
			iss >> op;
			if ("snd" == op) {
				send_calls++;
				iss >> tok;
				out->push(get_value(regs, tok));
			} else if ("set" == op) {
				iss >> reg;
				iss >> tok;
				regs[reg] = get_value(regs, tok);
			} else if ("add" == op) {
				iss >> tok;
				iss >> extra;
				set_value(regs, tok[0], get_value(regs, tok) + get_value(regs, extra));
			} else if ("mul" == op) {
				iss >> tok;
				iss >> extra;
				set_value(regs, tok[0], get_value(regs, tok) * get_value(regs, extra));
			} else if ("mod" == op) {
				iss >> tok;
				iss >> extra;
				set_value(regs, tok[0], get_value(regs, tok) % get_value(regs, extra));
			} else if ("rcv" == op) {
				iss >> reg;
				if (!in->empty()) {
					set_value(regs, reg, in->front());
					in->pop();
				} else {
					/* yield the cpu to the other machine */
					return;
				}
			} else if ("jgz" == op) {
				iss >> tok;
				iss >> extra;
				if (get_value(regs, tok) > 0) {
					pc_incr = get_value(regs, extra);
				}
			} else {
				std::cout << "Invalid instruction: " << curr << std::endl;
			}

			PC += pc_incr;
		}
	}

private:
	size_t PC;
	Regs regs{};
	const std::vector<std::string>& prog;
	std::shared_ptr<queue> in, out;
};

void second(const std::vector<std::string>& prog) {
	std::shared_ptr<queue> toZero{new queue()};
	std::shared_ptr<queue> toOne{new queue()};

	DuetMachine mzero{0, prog, toZero, toOne};
	DuetMachine mone{1, prog, toOne, toZero};

	do {
		mzero.execute();
		mone.execute();
	} while(!toZero->empty() || !toOne->empty());

	std::cout << "[2] 1 sent " << mone.send_calls << " values to 0." << std::endl;
}

int main() {
	std::istream& is = std::cin;
	Regs regs{};
	std::vector<std::string> program{};

	std::string line;
	while (std::getline(is, line)) {
		program.push_back(line);
	}
	std::cout << "Program size: " << program.size() << std::endl;

	first(program);
	second(program);
	return 0;
}