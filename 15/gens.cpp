#include <cassert>
#include <cstdint>
#include <iostream>

class Gen {
public:
	Gen(uint32_t f, uint32_t s, uint32_t _filter) :
			factor(f), filter(_filter),
			seed(s), previous(s) {}
	Gen(uint32_t f, uint32_t s) :
			factor(f), filter(1),
			seed(s), previous(s) {}
	~Gen() {}

	int next() {
		do {
			uint64_t product = ((uint64_t) factor) * ((uint64_t) previous);
			previous = (uint32_t) (product % 2147483647L);
		} while (previous % filter != 0);
		return previous;
	}

private:
	const uint32_t factor;
	const uint32_t filter;
	const uint32_t seed;
	uint32_t previous;
};

int read_starting_value(std::istream& in) {
	int value;
	std::string garbage;
	in >> garbage >> garbage >> garbage >> garbage;
	in >> value;
	return value;	
}

int first(const int seedA, const int seedB) {
	Gen genA{16807, (uint32_t) seedA};
	Gen genB{48271, (uint32_t) seedB};

	int equals = 0;
	for (int i = 0; i < 40000000; i++) {
		if ((genA.next()&0xffff) == (genB.next()&0xffff)) {
			equals++;
		}
	}

	return equals;
}

int second(const int seedA, const int seedB) {
	Gen genA{16807, (uint32_t) seedA, 4};
	Gen genB{48271, (uint32_t) seedB, 8};

	int equals = 0;
	for (int i = 0; i < 5000000; i++) {
		if ((genA.next()&0xffff) == (genB.next()&0xffff)) {
			equals++;
		}
	}

	return equals;
}

void test_all() {
	assert(588 == first(65, 8921));
	assert(309 == second(65, 8921));
	std::cout << "\x1b[1;32mAll tests passed\x1b[0m" << std::endl;
}

int main() {
	//test_all();
	int seedA = read_starting_value(std::cin);
	int seedB = read_starting_value(std::cin);

	std::cout << "A's starting value: " << seedA << std::endl;
	std::cout << "B's starting value: " << seedB << std::endl;

	std::cout << "[1] Final count: " << first(seedA, seedB) << std::endl;
	std::cout << "[2] Final count: " << second(seedA, seedB) << std::endl;
	
	return 0;
}
