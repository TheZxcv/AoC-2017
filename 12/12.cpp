#include <algorithm>
#include <cassert>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <vector>

void visit(int id, const std::map<int, std::vector<int>>& edges, std::set<int>& visited) {
	visited.insert(id);
	
	for (int child : edges.find(id)->second) {
		if (visited.find(child) == visited.end()) {
			visit(child, edges, visited);
		}
	}
}

int main() {
	std::map<int, std::vector<int>> pipes{};
	std::string line;
	while (std::getline(std::cin, line)) {
		std::istringstream iss{line};

		int id;
		iss >> id;
		
		std::string word;
		iss >> word; // munching separator '<->'
		assert(word == "<->");
		while (iss >> word) {
			word.erase(std::remove(word.begin(), word.end(), ','), word.end());
			pipes[id].push_back(std::stoi(word));
		}
	}

	std::set<int> visited{};
	visit(0, pipes, visited);

	std::cout << "Visited: " << visited.size() << std::endl;

	int ngroups = 1;
	std::set<int> remaining_nodes{};
	for (auto pair : pipes) {
		int node_id = pair.first;
		if (visited.find(node_id) == visited.end()) {
			remaining_nodes.insert(node_id);
		}
	}

	while (remaining_nodes.size() != 0) {
		ngroups++;
		std::set<int> visited{};
		visit(*remaining_nodes.begin(), pipes, visited);

		std::set<int> result;
		std::set_difference(remaining_nodes.begin(), remaining_nodes.end(), visited.begin(), visited.end(), std::inserter(result, result.end()));
		remaining_nodes = result;
	}

	std::cout << "Groups found: " << ngroups << std::endl;

	return 0;
}
