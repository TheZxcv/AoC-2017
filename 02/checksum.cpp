#include <cassert>
#include <iostream>
#include <limits>
#include <string>
#include <sstream>
#include <vector>

int line_checksum_minmax(const std::string& line) {
	int curr;
	int min = std::numeric_limits<int>::max();
	int max = std::numeric_limits<int>::min();
	std::istringstream sline{line};
	while (sline >> curr) {
		if (curr < min) {
			min = curr;
		}
		if (curr > max) {
			max = curr;
		}
	}
	return max - min;
}

int line_checksum_divisibility(const std::string& line) {
	int small, large, curr;
	std::vector<int> vec{};
	std::istringstream sline{line};
	while (sline >> curr) {
		vec.push_back(curr);
	}
	
	bool found = false;
	for (int a : vec) {
		for (int b : vec) {
			if (a != b && b > a && b%a == 0) {
				large = b;
				small = a;
				found = true;
				break;
			}
		}
		if (found)
			break;
	}
	return large / small;
}

int checksum(std::istream &input, int (*line_checksum)(const std::string&)) {
	int sum = 0;
	std::string line;
	while (std::getline(input, line)) {
		sum += line_checksum(line);
	}
	return sum;
}

void check(int expected, const std::string& input, int (*line_checksum)(const std::string&)) {
	std::istringstream iss{input};
	assert(expected == checksum(iss, line_checksum));
}

void testAll() {
	check(18, "5 1 9 5\n7 5 3\n2 4 6 8\n", line_checksum_minmax);
	check(9, "5 9 2 8\n9 4 7 3\n3 8 6 5\n", line_checksum_divisibility);
}

int main(int argc, char *argv[]) {
	if (argc <= 1) {
		testAll();
	} else {
		std::string arg{argv[1]};
		if (arg.compare("1") == 0) {
			int sum = checksum(std::cin, line_checksum_minmax);
			std::cout << "Checksum: " << sum << std::endl;
		} else if (arg.compare("2") == 0) {
			int sum = checksum(std::cin, line_checksum_divisibility);
			std::cout << "Checksum: " << sum << std::endl;
		}
	}
	
	return 0;
}
