#include <algorithm>
#include <cassert>
#include <iostream>
#include <map>
#include <sstream>

template<class T, size_t N>
constexpr size_t size(T (&)[N]) { return N; }

using Regs = std::map<std::string, int>;

int get_value(const Regs& regs, const std::string& name) {
	auto it = regs.find(name);
	if (it == regs.end()) {
		return 0;
	} else {
		return it->second;
	}
}

void set_value(Regs& regs, const std::string& name, int val) {
	regs[name] = val;
}

const std::string comps[] = {
	">", "<", ">=", "<=", "==", "!="
};
static size_t comps_len = size(comps);

using Comparator = bool (*)(int, int);

Comparator comparators[] = {
	[] (int a, int b) { return a > b; },
	[] (int a, int b) { return a < b; },
	[] (int a, int b) { return a >= b; },
	[] (int a, int b) { return a <= b; },
	[] (int a, int b) { return a == b; },
	[] (int a, int b) { return a != b; }
};
static size_t comparators_len = size(comparators);

size_t convert(const std::string& id) {
	for (size_t i = 0; i < comps_len; i++) {
		if (comps[i] == id)
			return i;
	}
	abort();
}

bool compare(size_t icomp, int a, int b) {
	if (icomp < comparators_len) {
		return comparators[icomp](a, b);
	} else {
		abort();
	}
}

int execute(std::istream& is) {
	int max_val = 0;
	Regs regs;
	std::string line;
	while (std::getline(is, line)) {
		std::istringstream iss{line};

		std::string dst;
		std::string instr;
		int value;
		std::string keyword;
		std::string reg;
		std::string comparison;
		int threshold;

		iss >> dst >> instr >> value >> keyword;
		assert(keyword == "if");
		iss >> reg >> comparison >> threshold;

		assert(instr == "inc" || instr == "dec");
		bool atLeastOne = false;
		for (auto it : comps) {
			atLeastOne |= (it == comparison);
		}
		assert(atLeastOne);

		int val = get_value(regs, reg);
		if (compare(convert(comparison), val, threshold)) {
			if (instr == "inc") {
				set_value(regs, dst, get_value(regs, dst) + value);
			} else {
				set_value(regs, dst, get_value(regs, dst) - value);
			}
			if (get_value(regs, dst) > max_val) {
				max_val = get_value(regs, dst);
			}
		}	
	}

	auto max_reg = std::max_element(regs.begin(), regs.end(),
		[] (std::pair<std::string, int> a, std::pair<std::string, int> b) { return a.second < b.second; });
	std::cout << max_reg->first << ": " << max_reg->second << std::endl;
	std::cout << "Maximum seen value: " << max_val << std::endl;
	return 0;
}

int main() {
	execute(std::cin);
	return 0;
}
