#include <algorithm>
#include <iostream>
#include <map>
#include <string>

struct Point3 {
	int x, y, z;

	int distance(const Point3& p) const {
		return (abs(this->x - p.x) + abs(this->y - p.y) + abs(this->z - p.z)) / 2;
	}

	friend Point3& operator+=(Point3& a, const Point3& b) {
		a.x += b.x;
		a.y += b.y;
		a.z += b.z;
		return a;
	}

	friend std::ostream& operator<<(std::ostream& os, const Point3& p) {
		os << "<" << p.x << ", " << p.y << ", " << p.z << ">";
		return os;
	}
};

struct Node {
	int id;
	Point3 p{0,0,0};
	const Node *n;
	const Node *ne;
	const Node *se;
	const Node *s;
	const Node *sw;
	const Node *nw;

	Node(int id) : id(id) {
		n = ne = se = s = sw = nw = nullptr;
	}
	~Node() {}
};

int main() {
	std::map<std::string, Point3> moves{};
    moves["n"]  = Point3{0,  1, -1};
    moves["ne"] = Point3{1,  0, -1};
    moves["se"] = Point3{1, -1,  0};
    moves["s"]  = Point3{0, -1,  1};
    moves["sw"] = Point3{-1,  0,  1};
    moves["nw"] = Point3{-1,  1,  0};

	Point3 p{0, 0, 0};
	Point3 furthest{0, 0, 0};
	std::string dir;
	while (std::getline(std::cin, dir, ',')) {
		dir.erase(std::remove(dir.begin(), dir.end(), '\n'), dir.end());
		auto it = moves.find(dir);
		if (it != moves.end()) {
			p += it->second;

			if (p.distance(Point3{0,0,0}) > furthest.distance(Point3{0,0,0})) {
				furthest = p;
			} 
		} else {
			std::cout << "ignored: '" << dir << "'" << std::endl;
		}   
	}

	std::cout << "Destination: " << p << std::endl;
	std::cout << p.distance(Point3{0,0,0}) << std::endl;
	
	std::cout << "Furthest distance: " << furthest << std::endl;
	std::cout << furthest.distance(Point3{0,0,0}) << std::endl;

	return 0;
}
