#include <algorithm>
#include <array>
#include <cassert>
#include <iostream>
#include <numeric>
#include <sstream>
#include <set>

using std::begin;
using std::end;

template<typename T, size_t N>
void print(std::ostream& os, const std::array<T, N>& programs, int offset) {
	int i = offset;
	do {
		os << programs[i];
		i = (i + 1) % programs.size();
	} while(i != offset);
}

template<typename T, size_t N>
void dance(std::array<T, N>& programs, int& offset, const std::string& steps) {
	int val;
	size_t posA, posB;
	char A, B;
	char ch;
	std::istringstream iss{steps};
	while(iss >> ch) {
		switch(ch) {
			case 's':
				iss >> val;
				offset += programs.size() - (val % programs.size());
				offset %= programs.size();
				break;
			case 'x':
				iss >> posA;
				iss >> ch;
				assert(ch == '/');
				iss >> posB;

				std::iter_swap(
					std::next(begin(programs), (offset + posA) % programs.size()),
					std::next(begin(programs), (offset + posB) % programs.size())
				);
				break;
			case 'p':
				iss >> A;
				iss >> ch;
				assert(ch == '/');
				iss >> B;
				std::iter_swap(
					std::find(begin(programs), end(programs), A),
					std::find(begin(programs), end(programs), B)
				);
				break;
		}
	}
}

void first(const std::string& steps) {
	std::array<char, 16> programs{};
	std::iota(begin(programs), end(programs), 'a');

	int start = 0;
	dance(programs, start, steps);

	std::cout << "[1] Final permutation: ";
	print(std::cout, programs, start);
	std::cout << std::endl;
}

void second(const std::string& steps) {
	std::set<std::string> states{};
	std::array<char, 16> programs{};
	std::iota(begin(programs), end(programs), 'a');

	int start = 0;
	for (int i = 0; i < 1000000000%60; i++) {
		dance(programs, start, steps);
	}

	std::cout << "[2] Final permutation: ";
	print(std::cout, programs, start);
	std::cout << std::endl;
}

int main() {
	std::string steps;
	if (std::getline(std::cin, steps)) {
		first(steps);
		second(steps);
	}
	return 0;
}
