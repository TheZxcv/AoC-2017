#include <cassert>
#include <cctype>
#include <iostream>
#include <sstream>
#include <vector>

using std::begin;
using std::end;

enum Dir { UP, DOWN, LEFT, RIGHT };

int main() {
	std::vector<std::string> diagram{};

	std::string line;
	while (std::getline(std::cin, line)) {
		diagram.push_back(line);
	}

	size_t posX = 0;
	size_t posY = 0;

	assert(diagram.size() > 0);
	posX = diagram[0].find('|');
	assert(posX != std::string::npos);

	Dir dir = DOWN;
	std::ostringstream oss{};

	int nsteps = 0;
	while (posY < diagram.size() && posX < diagram[posY].size() && diagram[posY].at(posX) != ' ') {
		char ch = diagram[posY].at(posX);

		if (std::isalpha(ch)) {
			oss << ch;
		} else if (ch == '+') {
			if (dir != DOWN && (posY - 1) < diagram.size()
					&& (diagram[posY-1].at(posX) == '|' || std::isalpha(diagram[posY-1].at(posX))))
				dir = UP;
			else if (dir != UP && (posY + 1) < diagram.size()
					&& (diagram[posY+1].at(posX) == '|' || std::isalpha(diagram[posY+1].at(posX))))
				dir = DOWN;
			else if (dir != RIGHT && (posX - 1) < diagram[posY].size()
					&& (diagram[posY].at(posX-1) == '-' || std::isalpha(diagram[posY].at(posX-1))))
				dir = LEFT;
			else if (dir != LEFT && (posX + 1) < diagram[posY].size()
					&& (diagram[posY].at(posX+1) == '-' || std::isalpha(diagram[posY].at(posX+1))))
				dir = RIGHT;
			else
				abort();
		}

		switch (dir) {
			case UP:
				posY--;
				break;
			case DOWN:
				posY++;
				break;
			case LEFT:
				posX--;
				break;
			case RIGHT:
				posX++;
				break;
			default: abort();
		}
		nsteps++;
	}

	std::cout << "[1] Path: " << oss.str() << std::endl;
	std::cout << "[2] Steps: " << nsteps << std::endl;

	return 0;
}
